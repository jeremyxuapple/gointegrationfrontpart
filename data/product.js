const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
	id: String,
	name: String,
	regularPrice: String,
	image: String,	
});

const Product = mongoose.model('product', ProductSchema);

module.exports = Product;

