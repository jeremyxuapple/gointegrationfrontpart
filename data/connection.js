const mongoose = require('mongoose');

// ES6 Promise
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/interview');

mongoose.connection.once('open', function() {
	console.log('connected...');
}).on('error', function(error) {
	console.log('error:', error);
});