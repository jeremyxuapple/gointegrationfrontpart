import BestBuyWebService from './BestBuyWebService';
import ProductView from './productView';
import ShoppingCart from './ShoppingCart';

export default class App {

	constructor() {

		this.productsArray = null;
		
	 	this.initBestBuyService();
	 	this.productView = new ProductView();
	 	this.initSite = true;
    this.pageLoaded = false;

	 	$(document).on('click', '#cart', {theApp:this}, function(event){
			if(sessionStorage.getItem('quantity') === null){
				return;
			} else {
				$('#cartWindow').show();
				event.data.theApp.shoppingCart.generateCartView();
			}
		});
	}

	initBestBuyService() {
		this.bbs = new BestBuyWebService();
		this.bbs.getData(this);
	}

	// Populate data into the products section
	productsPopulate(productsArray, theApp) {
		
		this.productView.dataPopulate(productsArray, theApp);
		this.initShoppingCart();
	}

	initShoppingCart(){	
		this.shoppingCart = new ShoppingCart(this.productsArray, this);
		$(document).on('click', '#cartClose', function(){
			$('#cartWindow').hide();
		});
	}

} // Close of the app
