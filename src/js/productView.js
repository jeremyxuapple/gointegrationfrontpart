
export default class ProductView{

	constructor() {
		this.productsArray = null;
		this.productString = null;
		this.categoryString = null;
		this.app = null;
		this.output = "";
	}

	dataPopulate(productsArray, theApp){
		
		this.app = theApp;
		this.output ="";
		
		theApp.productsArray = productsArray;
		for(let i = 0; i < productsArray.length; i++) {

		this.output +=
		`<div class="product product item text-center product${i}" data-sku="${productsArray[i].id}">
				<img class="productImg" src="${productsArray[i].image.src}">
		  		
		  		<h4 class="productName lineHeight-regular">${productsArray[i].title}</h4>
		  		<p class="productPrice">$${productsArray[i].variants[0].price}</p>
		  		<div>
		  			
		  			<button id="insert-${productsArray[i].id}" class="addToCart">Add to Cart</button>
		  		</div>
		</div>`;
		}
				
				$("#productList").html(this.output);

				if( this.app.pageLoaded){
					this.owlCarousel();
				}
				
				if(!this.app.pageLoaded && this.app !== null) {
						window.addEventListener('load', this.carouselLoadedwithPage(this.app), false);					
				}
				
}


owlCarousel(){

	$('.owl-carousel').owlCarousel({
			    loop:true,
			    margin:10,
			    nav:true,
			    responsive:{
			        0:{
			            items:1
			        },
			        600:{
			            items:2
			        },
			        1000:{
			            items:4
			        }
			    },
			    });
	
} // end of owlCarousel()

// reloadStylesheets() {

//    var queryString = '?reload=' + new Date().getTime();
//    $('link[rel="stylesheet"]').each(function () {
   		
//    		if (this.href.indexOf("googleapis")==-1){
//    			// console.log(this);
//    			//this.addEventListener('load',function(e){console.log(this.href + ' loaded')},false);
//        this.href = this.href.replace(/\?.*|$/, queryString);
//    		}
//    });

// }

carouselLoadedwithPage(theApp){

  let callBackFunction = function(){
  	$('.owl-carousel').owlCarousel({
			    loop:true,
			    margin:10,
			    nav:true,
			    responsive:{
			        0:{
			            items:1
			        },
			        600:{
			            items:2
			        },
			        1000:{
			            items:4
			        }
			    },
			    });
  	theApp.pageLoaded = true;
  }; // end of callBackFunction()
  
  return callBackFunction();
} // end of carouselLoadedwithPage function




} // end of productView class
